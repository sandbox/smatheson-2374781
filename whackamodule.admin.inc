<?php

/**
 * @file
 * Admin page callbacks for the whackamodule module.
 */

/**
 * Array sorting callback; sorts modules or themes by their name.
 */
function whackamodule_sort_modules_by_info_name($a, $b) {
  return strcasecmp($a->info['name'], $b->info['name']);
}

/**
 * Remove functions
 */

/**
 * Builds a form of currently disabled modules.
 *
 * @ingroup forms
 * @see whackamodule_modules_remove_validate()
 * @see whackamodule_modules_remove_submit()
 * @param $form_state['values']
 *   Submitted form values.
 * @return
 *   A form array representing the currently disabled modules.
 */
function whackamodule_modules_remove($form, $form_state = NULL) {
  
  // Display the confirm form if any modules have been submitted.
  if (!empty($form_state['storage']) && $confirm_form = whackamodule_modules_remove_confirm_form($form_state['storage'])) {
    return $confirm_form;
  }

  // Get a list of disabled, installed modules.
  $all_modules = system_rebuild_module_data();

  $uninstalled_modules = array();
  foreach ($all_modules as $name => $module) {
    if (empty($module->status) && $module->schema_version == SCHEMA_UNINSTALLED) {
      $uninstalled_modules[$name] = $module;
    }
  }

  // Only build the rest of the form if there are any modules available to
  // remove.
  if (!empty($uninstalled_modules)) {
    $profile = drupal_get_profile();
    uasort($uninstalled_modules, 'whackamodule_sort_modules_by_info_name');
    $form['remove'] = array('#tree' => TRUE);
    foreach ($uninstalled_modules as $module) {
      $module_name = $module->info['name'] ? $module->info['name'] : $module->name;
      $form['modules'][$module->name]['#module_name'] = $module_name;
      $form['modules'][$module->name]['name']['#markup'] = $module_name;
      $form['modules'][$module->name]['description']['#markup'] = t($module->info['description']);
      $form['remove'][$module->name] = array(
        '#type' => 'checkbox',
        '#title' => t('Remove @module module', array('@module' => $module_name)),
        '#title_display' => 'invisible',
      );

    }
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Remove'),
    );
    $form['#action'] = url('admin/modules/remove/confirm');
  }
  else {
    $form['modules'] = array();
  }

  return $form;
}

/**
 * Confirm remove of selected modules.
 *
 * @ingroup forms
 * @param $storage
 *   An associative array of modules selected to be removed.
 * @return
 *   A form array representing modules to confirm.
 */
function whackamodule_modules_remove_confirm_form($storage) {
  // Nothing to build.
  if (empty($storage)) {
    return;
  }

  // Construct the hidden form elements and list items.
  foreach (array_filter($storage['remove']) as $module => $value) {
    $info = drupal_parse_info_file(drupal_get_path('module', $module) . '/' . $module . '.info');
    $remove[] = $info['name'];
    $form['remove'][$module] = array(
      '#type' => 'hidden',
      '#value' => 1,
    );
  }

  // Display a confirm form if modules have been selected.
  if (isset($remove)) {
    $form['#confirmed'] = TRUE;
    $form['remove']['#tree'] = TRUE;
    $form['modules'] = array(
      '#markup' => '<p>' . t('The following modules will be completely removed from your site, and <em>all files from these modules will be lost</em>!') . '</p>' . theme('item_list', array('items' => $remove))
    );
    $form = confirm_form(
      $form,
      t('Confirm remove'),
      'admin/modules/remove',
      t('Would you like to continue with removing the above?'),
      t('Remove'),
      t('Cancel'));
    return $form;
  }
}

/**
 * Validates the submitted remove form.
 */
function whackamodule_modules_remove_validate($form, &$form_state) {
  // Form submitted, but no modules selected.
  if (!count(array_filter($form_state['values']['remove']))) {
    drupal_set_message(t('No modules selected.'), 'error');
    drupal_goto('admin/modules/remove');
  }
}


/**
 * Removes a given list of uninstalled modules.
 *
 * @param array $module_list
 *   The modules to remove. It is the caller's responsibility to ensure that
 *   all modules in this list have already been uninstalled before this function
 *   is called.
 *
 * @return bool
 *   Returns TRUE if the operation succeeds or FALSE if it aborts due to an
 *   unsafe condition.
 *
 * @see module_uninstall()
 */
function whackamodule_remove_modules($module_list = array()) {

  //watchdog('whackamodule', 'Modules %modules.', array('%modules' => print_r($modules, TRUE) ), WATCHDOG_DEBUG);

  // Get all module data so we can find dependents and sort.
  $module_data = system_rebuild_module_data();

  //watchdog('whackamodule', 'Module data %data.', array('%data' => print_r($module_data, TRUE) ), WATCHDOG_DEBUG);
    
  foreach ($module_list as $index => $name) {
    if (isset($module_data[$name])) {
      $module = $module_data[$name];
      //watchdog('whackamodule', ':) %dir.', array('%dir' => print_r($module, TRUE) ), WATCHDOG_DEBUG);
      
      // There is nothing magical here.  Picked an arbitrary length path to keep from accidentally
      // whacking your entire drupal tree because you didn't know what you were calling
      if (!isset($module->uri) || (strlen($module->uri) <= strlen("modules/"))) {
        return FALSE;
      }
      else {
        $moduledir = dirname( DRUPAL_ROOT . '/' . $module->uri );

        //watchdog('whackamodule', '%dir directory whacked.', array('%dir' => $moduledir ), WATCHDOG_DEBUG);
        if (file_unmanaged_delete_recursive( $moduledir )) {
          watchdog('system', '%module module removed.', array('%module' => $name), WATCHDOG_INFO);
        }
      }
    }
  }
  return TRUE;    
}

/**
 * Processes the submitted remove form.
 */
function whackamodule_modules_remove_submit($form, &$form_state) {

  if (!empty($form['#confirmed'])) {
    // Call the remove routine for each selected module.
    $modules = array_keys($form_state['values']['remove']);
    whackamodule_remove_modules($modules);

    drupal_set_message(t('The selected modules have been removed.'));

    $form_state['redirect'] = 'admin/modules/remove';
  }
  else {
    $form_state['storage'] = $form_state['values'];
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Returns HTML for a table of currently uninstalled modules.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_whackamodule_modules_remove($variables) {
  $form = $variables['form'];

  // No theming for the confirm form.
  if (isset($form['confirm'])) {
    return drupal_render($form);
  }

  // Table headers.
  $header = array(t('Remove'),
    t('Name'),
    t('Description'),
  );

  // Display table.
  $rows = array();
  foreach (element_children($form['modules']) as $module) {
    if (!empty($form['modules'][$module]['#required_by'])) {
      $disabled_message = format_plural(count($form['modules'][$module]['#required_by']),
        'To remove @module, the following module must be uninstalled first: @required_modules',
        'To remove @module, the following modules must be uninstalled first: @required_modules',
        array('@module' => $form['modules'][$module]['#module_name'], '@required_modules' => implode(', ', $form['modules'][$module]['#required_by'])));
      $disabled_message = '<div class="admin-requirements">' . $disabled_message . '</div>';
    }
    else {
      $disabled_message = '';
    }
    $rows[] = array(
      array('data' => drupal_render($form['remove'][$module]), 'align' => 'center'),
      '<strong><label for="' . $form['remove'][$module]['#id'] . '">' . drupal_render($form['modules'][$module]['name']) . '</label></strong>',
      array('data' => drupal_render($form['modules'][$module]['description']) . $disabled_message, 'class' => array('description')),
    );
  }

  $output  = theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No modules are available to remove.')));
  $output .= drupal_render_children($form);

  return $output;
}
