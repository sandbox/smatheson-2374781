# Whack a Module

The whackamodule module allows users to completely remove all files and
directories in a module.

To remove a module, you must first uninstall it.

The module is useful in hosting environments where an administrator might not
have console access to completely clean up a module that has been uninstalled.
The module does the equivalent of:
    rm -r [site]/sites/all/modules/[selected modules]
